disp('1. FIFO');
disp('2. LIFO');
disp('3. SJF');

choice=input('Enter choice: ');

ps_list=[0.1,0.2,0.3,0.4,0.5]; %service rate
E_D=[];
E_Q=[];
E_U=[];

if choice==1
    disp('Single server Queue: FIFO ');
    for ps=ps_list
        [aDelay,cInQ,aUtil]=FIFO(6,0.2,ps);
        E_D=[E_D aDelay];
        E_Q=[E_Q cInQ];
        E_U=[E_U aUtil];
        %fprintf('%0.2f %0.2f %0.2f \n',aDelay,cInQ,aUtil);
    end
elseif choice==2
    disp('Single server Queue: LIFO ');
    for ps=ps_list
        [aDelay,cInQ,aUtil]=LIFO(6,0.2,ps);
        E_D=[E_D aDelay];
        E_Q=[E_Q cInQ];
        E_U=[E_U aUtil];
        %fprintf('%0.2f %0.2f %0.2f \n',aDelay,cInQ,aUtil);
    end
else
    disp('Single server Queue: SJF ');
    for ps=ps_list
        [aDelay,cInQ,aUtil]=SJF(6,0.2,ps);
        E_D=[E_D aDelay];
        E_Q=[E_Q cInQ];
        E_U=[E_U aUtil];
        %fprintf('%0.2f %0.2f %0.2f \n',aDelay,cInQ,aUtil);
    end
end

%{
for ps=ps_list
    [aDelay,cInQ,aUtil]=SJF(6,0.2,ps);
    E_D=[E_D aDelay];
    E_Q=[E_Q cInQ];
    E_U=[E_U aUtil];
    %fprintf('%0.2f %0.2f %0.2f \n',aDelay,cInQ,aUtil);
end
%}

figure(1);
plot(ps_list,E_D,'-go');
title('service rate vs E[D]');
xlabel('Service Rate');
ylabel('E[D]');

figure(2);
plot(ps_list,E_Q,'-ro');
title('service rate vs E[Q]');
xlabel('Service Rate');
ylabel('E[Q]');

figure(3);
plot(ps_list,E_U,'-mo');
title('service rate vs E[U]');
xlabel('Service Rate');
ylabel('E[U]');