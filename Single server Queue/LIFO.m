function [avgDelay,avgCustomerInQ,avgUtilization]=LIFO(n,pa,ps)% ps is the service rate and pa is the arrival rate

rand('state',0) % produce same sequence of random numbers every time you run the code..
%disp('This is a single server queue: LIFO')
 
% we need some data structure and statistical counters
serverBusy=0; % whether the server is busy or not
cDelay=0; % delay
customersServed=0; % customers served
Queue=[]; % the queue which is initially empty
 
nextEvent=0; % 0 - arrival 1 - departure

% initially there are no departure and arrival scheduled
nextArrival=realmax;
nextDeparture=realmax;
 
% simulation time
sTime=0;
totalDelay=0;
 
% we generate the initial arrival only
% service rates will be generated only when they are entering in the service
nextArrival=geometric(pa);
 
arrivalTime=[];
depertureTime=[];
Qt=0; %area under Q(t)
Bt=0; %area under B(t)

while customersServed<n
    % decide the next event
    if nextArrival<nextDeparture
        nextEvent=0;    
    else
        nextEvent=1;
    end
    
    %set previous data
    prevTime=sTime;
    
    pCInQueue=length(Queue);
    pServerStatus=serverBusy;
    pQt=Qt;
    pBt=Bt;
    
    if nextEvent==0 % next event arrival
        currentTime=nextArrival;
        %list of Arrival events
        arrivalTime=[arrivalTime nextArrival];
        % check if the server is busy or not
            % server idle :
                      % 1.set delay = 0 for this customer.
                      % 2.mark the server busy
                      % 3.schedule a departure time for this customer
            % server busy :
                      % 1. put the new job(arrival time) into the queue
            
		% generate next arrival
        if serverBusy == 0 %server idle
            cDelay=0;
            totalDelay=totalDelay+cDelay;
            nextDeparture=currentTime+geometric(ps);
            serverBusy=1;
 
        else %server busy
            Queue=[Queue currentTime];   
        end
        % generate next arrival
        nextArrival=currentTime+geometric(pa);
        
    else
        currentTime=nextDeparture;
        %list of deperture events
        depertureTime=[depertureTime nextDeparture];
        %nextDeparture
        nextDeparture=realmax;
        
        if isempty(Queue) %Queue is empty
            serverBusy=0;
        else %Queue is not empty
            nextDeparture=currentTime+geometric(ps);
            jobEnd=Queue(length(Queue));
            %disp(jobEnd);
            Queue(length(Queue))=[];
            cDelay=currentTime-jobEnd;
            totalDelay=totalDelay+cDelay; 
        end
        customersServed=customersServed+1;  
    end
    sTime=currentTime;
    Qt=pQt+(sTime-prevTime)*pCInQueue;
    Bt=pBt+(sTime-prevTime)*pServerStatus;
     
end
avgDelay=totalDelay/n;
avgCustomerInQ=Qt/sTime;
avgUtilization=Bt/sTime;

%{
disp('Arrival Time: ');
disp(arrivalTime);
disp('Deperture Time:');
disp(depertureTime);
disp('--------------Calculation--------------');
disp('Avrage Delay: ');
disp(avgDelay);
disp('Expected no. of customers in Queue: ');
disp(avgCustomerInQ);
disp('Expected utilization of the server: ');
disp(avgUtilization);
%}
end