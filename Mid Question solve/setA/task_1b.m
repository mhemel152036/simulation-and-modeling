no_of_exp=10:20:1000;
a=1;
b=6;
hit=0;
actual_value=0.1642;
error=zeros(1,length(no_of_exp));
j=1;

for trial=no_of_exp
    dice1=randi([a,b],1,trial);
    dice2=randi([a,b],1,trial);
    sum_of_dice=dice1+dice2;
    
    for i=1:trial
        if sum_of_dice(i)==7
           hit=hit+1; 
        end
    end
    error(j)=abs(actual_value-(hit/trial));
    j=j+1;
    hit=0;
end
plot(1:length(error),error);
%axis([0 length(error) 0 1]);
xlabel('trail');
ylabel('error');