clear;
no_of_trial=input('Enter the no. of trial: ');
a=1;
b=6;
hit=0;
i=1;

dice1=randi([a,b],1,no_of_trial);
dice2=randi([a,b],1,no_of_trial);
sum_of_dice=dice1+dice2;

while i<=no_of_trial
    if sum_of_dice(i)==7
        hit=hit+1;
    end
    i=i+1;
end
format long;
fprintf('Experimental probability: %f\n',hit/no_of_trial);
