Ax=0;
Ay=0;
Va=60;
deltaDa=Va*1; % distance = speed x 1;

plot(Ax,Ay,'r.',...
    'markerSize',30);

hold on;

Bx=10000;
By=10000;
Vb=50;
deltaDb=Vb*1; % distance = speed x 1;

plot(Bx,By,'g.',...
    'markerSize',30);

attack_distance=1000;
time_limit=300;

for i=1:time_limit
    dis=sqrt((Bx-Ax)^2+(By-Ay)^2);
    
    if dis<=attack_distance
        disp('defense A wins.');
        break;
    else
        Bx=Bx-deltaDb;
        plot(Bx,By,'g.',...
            'markerSize',30);
        dis=sqrt((Bx-Ax)^2+(By-Ay)^2);
        Ax=Ax+deltaDa*((Bx-Ax)/dis);
        Ay=Ay+deltaDa*((By-Ay)/dis);
        
        plot(Ax,Ay,'r.',...
            'markerSize',30);
    end
end
grid on;