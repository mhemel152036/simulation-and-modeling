Ax=0;
Ay=0;
Va=60;
deltaDa=Va*1; %distance=speed X t
plot(Ax,Ay,'ro');
hold on;

Bx=10000;
By=10000;
Vb=50;
deltaDb=Vb*1; %distance=speed X t
plot(Bx,By,'go');

attack_distance=1000;
time_limit=3000;
flag=0;

for i=1:time_limit
    dis=sqrt((Bx-Ax)^2+(By-Ay)^2);
    if dis<=attack_distance
        %disp('Win: A');
        flag=1;
        break;
    else
        Bx=Bx-deltaDb;
        plot(Bx,By,'go');
        
        dis=sqrt((Bx-Ax)^2+(By-Ay)^2);
        Ax=Ax+deltaDa*((Bx-Ax)/dis);
        Ay=Ay+deltaDa*((By-Ay)/dis);
        
        plot(Ax,Ay,'ro');
    end
   
end

if flag==1
    disp('A win');
else
    disp('A lose');
end
