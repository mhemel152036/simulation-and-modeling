clear;
%Position of A (0,1000)
Ax=0;
Ay=1000;
Va=60;
deltaDa=Va*1; %distance=speed X t
plot(Ax,Ay,'ro');
hold on;

%Position of B (12000,2000)
Bx=12000;
By=2000;
Vb=60;
deltaDb=Vb*1; %distance=speed X t
plot(Bx,By,'go');

%Position of C (10000,10000)
Cx=10000;
Cy=10000;
Vc=60;
deltaDc=Vc*1; %distance=speed X t
plot(Cx,Cy,'bo');

%Position of D (5000,15000)
Dx=5000;
Dy=15000;
Vd=60;
deltaDd=Vd*1; %distance=speed X t
plot(Dx,Dy,'mo');

attack_distance=1000;
time_limit=300;
flagC=0;
flagB=0;
flagA=0;

for i=1:time_limit
    disCD=sqrt((Dx-Cx)^2+(Dy-Cy)^2);
    
    if disCD<=attack_distance
        flagC=1;
        break;
        
    else
        Dx=Dx-deltaDd;
        plot(Dx,Dy,'mo');
        
        disCD=sqrt((Dx-Cx)^2+(Dy-Cy)^2);
        
        Cx=Cx+deltaDc*((Dx-Cx)/disCD);
        Cy=Cy+deltaDc*((Dy-Cy)/disCD);
        plot(Cx,Cy,'bo');
    end
    
    
    
    disBC=sqrt((Cx-Bx)^2+(Cy-By)^2);
    if disBC<=attack_distance
        flagB=1;
        break;    
    else        
        Bx=Bx+deltaDb*((Cx-Bx)/disBC);
        By=By+deltaDb*((Cy-By)/disBC);
        plot(Bx,By,'go');
    end
    
    
    disAB=sqrt((Bx-Ax)^2+(By-Ay)^2);
    if disAB<=attack_distance
        %disp('Win: A');
        flagA=1;
        break;
    else  
        Ax=Ax+deltaDa*((Bx-Ax)/disAB);
        Ay=Ay+deltaDa*((By-Ay)/disAB);
        plot(Ax,Ay,'ro');
    end
 
end

axis([-10000 15000 0 15000]);

if flagC==1
    disp('C: win');
elseif flagB==1
    disp('B: win');
elseif flagA==1
    disp('A: win');
end