no_of_exp=10:10:10000;

actual=1/6;
error=zeros(1,length(no_of_exp));
j=1;

for trial=no_of_exp
    hit=0;
    for i=1:trial
        dice1=randi(6);
        dice2=randi(6);

        if dice1+dice2==7
           hit=hit+1; 
        end
    end
    est_val=hit/trial;
    error(j)=abs(actual-est_val);
    j=j+1;
end
plot(1:length(error),error);
