clear;
trial=input('Input no. of trial: ');

x=linspace(-2,2,100);
x_squre=x.^2;
y=sqrt(4-x_squre);
plot(x,y);
hold on;


x=linspace(-2,0,100);
plot(x,-x-2);

x=linspace(0,2,100);
plot(x,x-2);

xL = xlim;
yL = ylim;
line([0 0], yL);  %x-axis
line(xL, [0 0]);  %y-axis

a=-2;
b=2;
hit=0;

for i=1:trial
    x=a+rand()*(b-a);
    y=a+rand()*(b-a);
    
    if y>=0 && x^2+y^2<=2^2
        hit=hit+1;
        plot(x,y,'g.');
        
    elseif x<=0 && (y>=-x-2 && y<0)
       hit=hit+1;
       plot(x,y,'y.');
       
    elseif x>0 && (y>=x-2 && y<0)
       hit=hit+1;
       plot(x,y,'c.');
       
    else
        plot(x,y,'r.'); 
    end
end
axis([-2,2,-2,2]);
I=(hit/trial)*(b-a);
fprintf('Area of the shaded part: %0.4f\n',I);


