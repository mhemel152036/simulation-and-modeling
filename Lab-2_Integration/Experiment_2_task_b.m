no_of_exp=10:10:50000;

x=0:pi/20:2*pi;
y=sin(y);

y_pos_max=max(y);
y_neg_max=min(y);

error=zeros(1,length(no_of_exp));
j=1;

for trial=no_of_exp
    
    trial_neg=0;
    trial_pos=0;
    hit_pos=0;
    hit_neg=0;
    
   for i=1:trial
       r=rand();
       x=a+(b-a)*r;

       if x<=pi
           trial_pos=trial_pos+1;
           y_pos=rand();

           if y_pos<sin(x)
               hit_pos=hit_pos+1;
           end
       else
           trial_neg=trial_neg+1;
           y_neg=-rand();

           if y_neg>sin(x)
               hit_neg=hit_neg+1;
           end
       end
   end
  i_pos=(hit_pos/trial_pos)*((pi-0)*y_pos_max);
  i_neg=(hit_neg/trial_neg)*((2*pi-pi)*y_neg_max);
  I=i_pos+i_neg;
  error(j)=abs(0-I);
  j=j+1;
end

plot(1:length(error),error);
axis([0 length(error) 0 1]);
xlabel('trial');
ylabel('error');


