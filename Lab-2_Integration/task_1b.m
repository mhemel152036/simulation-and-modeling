no_of_trial=10:10:10000;

x=linspace(-3,5,100);
y=x.^2+5;

y_max=ceil(max(y));
a=-3;
b=5;
area=y_max*(b-a);


hit_count=0;
errors=zeros(1,length(no_of_trial));
j=1; %index of error list
actual_value=90.66;

for trial=no_of_trial
    for i=1:trial
        r1=rand();
        r2=rand();
        x=a+(b-a)*r1;
        y=y_max*r2;
        fx=x^2+5;
        
        if y<fx
            hit_count=hit_count+1;
        end
    end
    errors(j)=abs(actual_value-(hit_count*area/trial));
    j=j+1;
    hit_count=0;
end

plot(1:length(errors),errors);
grid on;
xlabel('trial number');
ylabel('errors');