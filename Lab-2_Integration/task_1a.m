no_of_trial=input('Input no of Trial: ');

x=linspace(-3,5,100);
y=x.^2+5;
plot(x,y,'b');
axis([-3,5,0,30]);
hold on;

y_max=ceil(max(y));
a=-3;
b=5;
area=y_max*abs(b-a);
hit_count=0;

for i=1:no_of_trial
    r1=rand();
    r2=rand();
    x=a+(b-a)*r1;
    y=y_max*r2;
    fx=x^2+5;
    
    if y<fx
        hit_count=hit_count+1;
        plot(x,y,'g.');
        hold on;
    else
        plot(x,y,'r.'); 
    end
end

disp(hit_count*area/no_of_trial);


