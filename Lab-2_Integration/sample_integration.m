%{
    Find the integral value of f(x)=x^2 function using Monte Carlo 
    Simulation method.

    Algorithm:
    ----------

    Step1: 
        Find a(lower limit), b(upper limit), area(Area of the region) 
        and y_max(maximum possible of value of the given function for the 
        given range. e.g. 5~10 )

    Step2:
        Find two random values r1=rand() and r2=rand() of range [0~1]

    Step3:
        Map random values into the corrosponding area,
        x= a+(b-a)*r1 ( Map all r1 random values with respect to x-axis )
        y= y_max*r2 ( Map all r2 random values with respect to y-axis )

    Step4:
        Find f(x)=x^2

        if y<f(x)         
            hit=hit+1;

    Step5:
        estimated_value=area*hit/trial
%}

no_of_trail=input('Enter no. of Trial: ');

%optional
x=linspace(0,10,100);
y=x.^2;
y_max=ceil(max(y)); %maximum possbile value of y
a=5; %lower limit
b=10; %upper limit

plot(x,y,'b');
grid on;
hold on;

area=(b-a)*y_max; %area of the reactangle
hit_count=0;

for i=1:no_of_trail
    r1=rand();
    r2=rand();
    x=a+(b-a)*r1;
    y=y_max*r2;
    
    fx=x^2; %find f(x)=x^2
    
    if y<fx
        plot(x,y,'g.');
        hold on;
        hit_count=hit_count+1;
    else
        plot(x,y,'r.');
        hold on;
    end
end

estimated_value=(area*hit_count)/no_of_trail;
disp(estimated_value);

