no_of_trial=input('Enter no. of trial: ');

x=0:pi/20:2*pi;
y=sin(x);
plot(x,y);
axis([0 6.35 -1 1]);
hold on;

y_pos_max=max(y);
y_neg_max=min(y);


a=0;
b=2*pi;
trial_pos=0;
trial_neg=0;

hit_pos=0;
hit_neg=0;

for i=1:no_of_trial
   r=rand();
   x=a+(b-a)*r;
   
   if x<=pi
       trial_pos=trial_pos+1;
       y_pos=rand();
       
       if y_pos<sin(x)
           hit_pos=hit_pos+1;
           plot(x,y_pos,'g.');
           hold on;
       else
           plot(x,y_pos,'r.');
           hold on;
       end
   else
       trial_neg=trial_neg+1;
       y_neg=-rand();
       
       if y_neg>sin(x)
           hit_neg=hit_neg+1;
           plot(x,y_neg,'g.');
           hold on;
       else
           plot(x,y_neg,'r.');
           hold on;
       end
   end
end

i_pos=(hit_pos/trial_pos)*((pi-0)*y_pos_max);
i_neg=(hit_neg/trial_neg)*((2*pi-pi)*y_neg_max);
I=i_pos+i_neg;
disp(I);


