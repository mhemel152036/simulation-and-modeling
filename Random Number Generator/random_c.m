p=11;
q=19;
m=p*q;
x0=3;
xi=[x0];
iteration=[1];
bi=[0];

for i=2:20
    iteration=[iteration;i];
    x=rem(x0^2,m);
    x0=x;
    xi=[xi;x];
    xbin=de2bi(x);
    count=sum(xbin(:)==1);
    if rem(count,2)==0
        bi=[bi;0];
    else
        bi=[bi;1];
    end
end
t=table(iteration,xi,bi);
disp(t);
temp=[];
k=1;
bit_len=4;
disp('-----------Random Numbers-----------');
for i=1:5
    temp=[bi(k) bi(k+1) bi(k+2) bi(k+3)];
    disp(temp);
    value=bi(k)*2^3+bi(k+1)*2^2+bi(k+2)*2^1+bi(k+3)*2^0;
    disp(value);
    k=k+bit_len;
end

