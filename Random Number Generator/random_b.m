format long;

z0=7;
zi=[z0];
iteration=[0];
ui=[0];
c=3;
m=16;
a=5;

for i=1:19
    iteration=[iteration;i];
    z=rem((a*z0+c),m);
    z0=z;
 
    ui=[ui;z/m];   
    zi=[zi;z];
    
    %zi_squ=[zi_squ;];
end
t=table(iteration,zi,ui);
disp(t);
