clear all;
M=11;
N=5;
init_inv=3;
odr_val=8;
init_arvl_day=1;
%demand table
demand=[0;1;2;3;4];
rand_dem_range0=[1;11;36;71;92];
rand_dem_range1=[10;35;70;91;100];
t1=table(demand,rand_dem_range0,rand_dem_range1);
disp(t1);
%lead table
lead_time=[1;2;3];
rand_lead_rang0=[1;7;0];
rand_lead_rang1=[6;9;0];

t2=table(lead_time,rand_lead_rang0,rand_lead_rang1);
disp(t2);
%main table
cycle=[];
days=[];
beg_inv=[init_inv];
rand_num=[];
dem=[];
end_inv=[];
sht_qun=[];
odr_qun=[];
lead_rand_digit=[];
days_arrive_order=[];
%required variable
cycle_time=5;
demand_val=0;
end_val=0;
sht_val=0;
random_list=[24;35;65;81;54;3;87;27;73;70;47;45;48;17;9];
lead_val=0;
lead_day=[];

for I=1:5
    for j=1:cycle_time
        cycle=[cycle;i];
        days=[days;j];
        
        rand_val=rand(100);
        rand_num=[rand_num;rand_val];
        if init_arvl_day==0
            init_inv=init_inv+odr_val;
        end
        init_arvl_day=init_arvl_day-1;
        
        for k=1:length(demand)
            if (rand_dem_range0(k)<=rand_val<=rand_dem_range0(k))
                demand_val=demand(k);
                dem=[dem;demand_val];
                if init_inv>demand_val
                    end_val=init_inv-demand_val;
                    sht_val=0;
                    %sht_qun=[sht_qun;sht_val];
                else
                    end_val=0;
                    sht_val=demand_val-init_inv;
                    %sht_qun=[sht_qun;sht_val];
                end
                sht_qun=[sht_qun;sht_val];
                end_inv=[end_inv;end_val];
                if init_arvl_day==0
                    init_inv=end_val+odr_val;
                else
                    init_inv=end_val;
                end
                beg_inv=[beg_inv;init_inv];
                break; 
            end
        end
        if j==5
            odr_val=M-end_val;
            odr_qun=[odr_qun;odr_val];
            rand_val=rand(9);
            lead_rand_digit=[lead_rand_digit;rand_val];
            for i=1:3
                if rand_val<=rand_lead_rang1(i)
                    lead_val=lead_time(i);
                    lead_day=[lead_day;lead_val];
                    init_inv=init_inv+odr_val;
                    break;
                end
            end
        else
            if lead_val>=1
                lead_day=[lead_day;lead_val];
            else
                lead_day=[lead_day;0];
            end
            lead_rand_digit=[lead_rand_digit,0];
            odr_qun=[odr_qun];
        end
    end
end
beg_inv=beg_inv(1:25);
t3=table(cycle,days,beg_inv,rand_num,demand,end_inv,sht_qun,rand_lead_time,lead_day);
