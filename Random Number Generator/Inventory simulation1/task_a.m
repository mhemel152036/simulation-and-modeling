clear all;
M=11;
N=5;
init_inv=3;
beg_inv=[init_inv];
order=8;

%demand table
Demand=[0; 1; 2; 3; 4];
ran_demand0=[1; 11; 36; 71; 92];
ran_demand1=[10; 35; 70; 91; 100];
t1=table(Demand,ran_demand0,ran_demand1);
disp(t1);

%table for lead time
Lead_time=[1;2;3];
rand_lead_time0=[0;1;7];
rand_lead_time1=[6;9;0];
t2=table(Lead_time,rand_lead_time0,rand_lead_time1);
disp(t2);

cycle_len=5;
day_len=5;
end_inv=[];
end_val=0;
shr_qua=[];
ord_qua=[];
rand_val=0;
rand_val1=0;
ran_leadtime=[];

cycle=[];
days=[];
rand_demand=[];
demand=[];
day_until=[];


lt=1;
sht_val=0;
dem_val=0;
sht_day=0;
%random_list=[24;35;65;81;54;3;87;27;73;70;47;45;48;17;9];
%random_list1=[0;0;0;0;5;0;0;0;0;0;0;0;0;0;3];

for i=1:cycle_len
    for j=1:day_len
         cycle=[cycle;i];
         days=[days;j];
         
         rand_val=randi(100);
         rand_demand=[rand_demand;rand_val];
         if lt==-1
             init_inv=init_inv+order;
         end   
         
         lt=lt-1;
        
        for k=1:N
            if rand_val<=ran_demand1(k)
                dem_val=Demand(k);
                if dem_val+sht_val<=init_inv
                    
                    totalDemand=dem_val+sht_val;
                    end_val=(init_inv-totalDemand);
                    sht_val=0;
                    %shr_qua=[shr_qua;sht_val];
                else
                    sht_val=sht_val+(dem_val-init_inv);
                    end_val=0;
                    sht_day=sht_day+1;
                    %shr_qua=[shr_qua;sht_val];
                end
                shr_qua=[shr_qua;sht_val];
                end_inv=[end_inv;end_val];
                
                if lt==-1
                    beg_inv=[beg_inv;end_val+order];
                else
                    beg_inv=[beg_inv;end_val];
                end
                
                init_inv=end_val;
                demand=[demand;Demand(k)];
                break;
            end 
        end
        
        if j==5
             order=M-end_val; 
             ord_qua=[ord_qua; order];
             rand_val1=randi(9);
            
             ran_leadtime=[ran_leadtime; rand_val1];
             
             if rand_val1<=rand_lead_time1(1)
                lt=Lead_time(1);
             elseif rand_val1<=rand_lead_time1(2)
                 lt=Lead_time(2);
             elseif rand_val1<=rand_lead_time1(3)
                 lt=Lead_time(3);    
             end
             day_until=[day_until; lt]; 
         else
             if lt>=1
                 day_until=[day_until;lt];
             else
                 day_until=[day_until;0];
             end
             ran_leadtime=[ran_leadtime;0];
             ord_qua=[ord_qua;0];
             
        end
    end
end

beg_inv=beg_inv(1:25);
t3=table(cycle,days,beg_inv,rand_demand,demand,end_inv,shr_qua,ord_qua,ran_leadtime,day_until);
disp(t3);


total_ending=sum(end_inv);
fprintf('Avg ending: %0.3f\n',total_ending/25);
fprintf('Total shortage days: %d\n',sht_day);
