clear;
no_of_trial=input('Enter no of trial: ');

i=0;
imax=6;
point_count=zeros(1,imax);

while i<no_of_trial
    point=randi(imax); %generate random points between 1~6
    %disp(point);
    
    %point checking and counting occurance
    if point==1
        point_count(point)=point_count(point)+1;
    elseif point==2
        point_count(point)=point_count(point)+1;
    elseif point==3
        point_count(point)=point_count(point)+1;
    elseif point==4
        point_count(point)=point_count(point)+1;
    elseif point==5
        point_count(point)=point_count(point)+1;  
    elseif point==6
        point_count(point)=point_count(point)+1; 
    end
   i=i+1; 
end
point_probability=point_count*(1/no_of_trial); %set probability to each points

plot(1:length(point_probability),point_probability,'m-')
axis([1,6, 0,1])
