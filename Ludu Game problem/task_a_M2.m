clear;
no_of_trial=input('Enter No. of trial: ');

%generate random point list 
points=randi([1,6],1,no_of_trial);
point=6;

%list of size 1x6
probability=zeros(1,point);

for i=points
   probability(i)=probability(i)+(1/no_of_trial);
end

plot(1:length(probability),probability);
axis([1,6,0,1]);
grid on;