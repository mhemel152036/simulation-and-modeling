prompt = 'Enter Trial Number: ';
trial = input(prompt);
imax = 6;
x = [1:6];
i = 1;
y = zeros(1,6);
while i <= trial
    r = randi(imax);
    if r == 1
        y(1) = y(1) + 1/trial;
    elseif r == 2
        y(2) = y(2) + 1/trial;
    elseif r == 3
        y(3) = y(3) + 1/trial;
    elseif r == 4
        y(4) = y(4) + 1/trial;
    elseif r == 5
        y(5) = y(5) + 1/trial;
    else
        y(6) = y(6) + 1/trial;
    end
    i = i + 1;
        
end
plot(x,y);
grid on;
xlabel('diceNumber');