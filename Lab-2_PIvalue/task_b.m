no_of_trials=10:10:100000;

hit_count=0;
%{
x=linspace(0,1,200);
x_squre=x.^2;
y=sqrt(1-x_squre);
plot(x,y);
hold on;
%}

errors=zeros(1,length(no_of_trials));
j=1;

for trial=no_of_trials
    for i=1:trial
        pos_x=rand();
        pos_y=rand();
        
        if pos_x^2+pos_y^2<=1
            hit_count=hit_count+1;
            %{
            plot(pos_x,pos_y,'m.');
            hold on;
            
        else
            plot(pos_x,pos_y,'g.');
            hold on;
            %}
        end
    end
    pi_value=4*(hit_count/trial);
    errors(j)=abs(pi-pi_value);
    j=j+1;
    hit_count=0;
end
plot(1:length(no_of_trials),errors,'b');
axis([1,length(errors),0,1]);
xlabel('trial numbers');
ylabel('errors');
