trial=input('Enter trial Number: ');

hit_count=0;

x=linspace(0,1,200);
x_squre=x.^2;
plot(x,sqrt(1-x_squre))
hold on

for i=1:trial
   pos_x=rand();
   pos_y=rand();
   
   if (pos_x^2+pos_y^2)<=1
       hit_count=hit_count+1;
       plot(pos_x,pos_y,'m.');
       hold on;
   else
       plot(pos_x,pos_y,'g.');
       hold on;
   end
end
fprintf('Value of PI: %f\n',4*(hit_count/trial));