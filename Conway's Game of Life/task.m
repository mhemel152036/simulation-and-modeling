rc=input('Enter no. of row and column: ');
row=rc;
col=rc;
n=input('Enter no of generation: ');

gen1=zeros(row,col);
gen1(2,3:col)=1;
gen1(3,2:col-1)=1;

gen2=zeros(row,col);


count=0;





disp('Generation-1: ');
disp(gen1);


for k=1:n-1
    for i=2:row-1
        for j=2:col-1
            if gen1(i-1,j-1)==1
                count=count+1;
            end
            if gen1(i-1,j)==1
                count=count+1;
            end
            if gen1(i-1,j+1)==1
                count=count+1;
            end
            
            if gen1(i,j-1)==1
                count=count+1;
            end
            if gen1(i,j+1)==1
                count=count+1;
            end
            
            if gen1(i+1,j-1)==1
                count=count+1;
            end
            if gen1(i+1,j)==1
                count=count+1;
            end
            if gen1(i+1,j+1)==1
                count=count+1;
            end
            %-----------neighbour checking----------
            
            if gen1(i,j)==1 %if alive
                if count<2
                    gen2(i,j)=0; %make dead
                end
                if count==2 || count==3
                    gen2(i,j)=1; %make alive
                end
                if count>3
                    gen2(i,j)=0; %make dead
                end
                
            else   %if dead
                if count==3
                    gen2(i,j)=1;  %make alive
                end
            end
            count=0;
             
        end
    end
    
    %--------------row-1----------
    i=1;
    for j=2:col-1
        if gen1(i,j-1)==1
            count=count+1;
        end
        
        if gen1(i,j+1)==1
            count=count+1;
        end
        
        
        if gen1(i+1,j-1)==1
            count=count+1;
        end
        if gen1(i+1,j)==1
            count=count+1;
        end
        if gen1(i+1,j+1)==1
            count=count+1;
        end
        
        if gen1(i,j)==1 %if alive
            if count<2
                gen2(i,j)=0;
            end
            if count==2 || count==3
                gen2(i,j)=1;
            end
            if count>3
                gen2(i,j)=0;
            end
        else  %if dead
            if count==3
                gen2(i,j)=1;
            end
        end
        count=0;
    end
    
    
    %--------------row-5----------
    i=5;
    for j=2:col-1
        if gen1(i,j-1)==1
            count=count+1;
        end
        
        if gen1(i,j+1)==1
            count=count+1;
        end
        
        
        if gen1(i-1,j-1)==1
            count=count+1;
        end
        if gen1(i-1,j)==1
            count=count+1;
        end
        if gen1(i-1,j+1)==1
            count=count+1;
        end
        
        if gen1(i,j)==1 %if alive
            if count<2
                gen2(i,j)=0;
            end
            if count==2 || count==3
                gen2(i,j)=1;
            end
            if count>3
                gen2(i,j)=0;
            end
        else  %if dead
            if count==3
                gen2(i,j)=1;
            end
        end
        count=0;
    end
    
    %--------column-----------------
    
    j=1;
    for i=2:row-1
        if gen1(i-1,j)==1
            count=count+1;
        end
        if gen1(i+1,j)==1
            count=count+1;
        end
        
        if gen1(i-1,j+1)==1
            count=count+1;
        end
        if gen1(i,j+1)==1
            count=count+1;
        end
        if gen1(i+1,j+1)==1
            count=count+1;
        end
       if gen1(i,j)==1 %if alive
            if count<2
                gen2(i,j)=0;
            end
            if count==2 || count==3
                gen2(i,j)=1;
            end
            if count>3
                gen2(i,j)=0;
            end
        else  %if dead
            if count==3
                gen2(i,j)=1;
            end
        end
        count=0;   
    end
    
    
    %--------column-5-----------------
    
    j=5;
    for i=2:row-1
        if gen1(i-1,j)==1
            count=count+1;
        end
        if gen1(i+1,j)==1
            count=count+1;
        end
        
        if gen1(i-1,j-1)==1
            count=count+1;
        end
        if gen1(i,j-1)==1
            count=count+1;
        end
        if gen1(i+1,j-1)==1
            count=count+1;
        end
       if gen1(i,j)==1 %if alive
            if count<2
                gen2(i,j)=0;
            end
            if count==2 || count==3
                gen2(i,j)=1;
            end
            if count>3
                gen2(i,j)=0;
            end
        else  %if dead
            if count==3
                gen2(i,j)=1;
            end
        end
        count=0;   
    end
    
    fprintf('Generation- %d\n',k+1);
    disp(gen2);
    gen1=gen2;
end


